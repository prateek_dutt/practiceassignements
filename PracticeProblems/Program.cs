﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticeProblems
{
    class Program
    {



        static void Main(string[] args)
        {

            takeAction(displayMenu());
            Console.Read();


            //takeAction(displayMenu())
            //Console.WriteLine("Enter Y to Continue and N to Exit");

            newMethod(); //My New Method
            secondMethod();//My Second New Method




        }

        private static void secondMethod()
        {
            throw new NotImplementedException();
        }

        private static void newMethod()
        {
            throw new NotImplementedException();
        }

        private static int displayMenu()
        {
            int choice;
            Console.WriteLine("****** PROGRAM MENU*******");
            Console.WriteLine("____________________________");
            Console.WriteLine("\n\n 1. Insertion Sort");
            Console.WriteLine(" 2. Bubble Sort");
            Console.WriteLine("3. Validate USDN");
            Console.WriteLine("4. MultiDimArray ");
            
            Console.WriteLine("\n__________________________");
            Console.WriteLine("\n\nEnter your choice");
            choice = Convert.ToInt32(Console.ReadLine());


            return choice;



        }
        private static void takeAction(int choice)
        {
            switch (choice)
            {
                case 1:
                    insertSort();
                    
                    break;

                case 2:
                    bubbleSort();
                    break;
                
                case 3:
                    validateUSDN();
                    break;
                
            }
        }
        private static void checkUserResp()
        {
            Console.WriteLine("\n\nEnter Y to Continue to Main Menu and N to exit");
            string resp = Convert.ToString(Console.ReadLine());
            if (resp.Equals("Y") || resp.Equals("y"))
            {
                takeAction(displayMenu());
            }
            else
            {
                exit();
            }

        }
        private static void exit()
        {
            Console.WriteLine("Thank you for using this program*******");
            return;
        }
        private static void insertSort()
        {
            int [] arr=takeArrayInput();
            insertionSort(arr);
            printArray(arr);
            
        }

        private static void printArray(int[] arr)
        {
            Console.WriteLine("\n Printing Sorted Array");
            for(int i=0;i<arr.Length;i++)
            {
                Console.Write(arr[i]+"\t");
            }
            checkUserResp();
        }


        private static void bubbleSort()
        {
            int[] arr = takeArrayInput();
            int i = 0;
            int j = 0;
            int N = arr.Length;
            for(;i<N-1;i++)
            {
                for(j=0;j<N-i-1;j++)
                {
                    if (arr[j] > arr[j+1])
                    {
                        Swap(arr, arr[j], arr[j + 1],j);
                    }
                }
            }
            printArray(arr);

        }

        private static void Swap(int[] arr, int v1, int v2,int j)
        {
            int temp = v1;
            arr[j] = v2;
            arr[j + 1] = v1;
        }

        private static void insertionSort(int[] arr)
        {

            for(int i=0;i<arr.Length;i++)
            {
                int temp = arr[i];
                int j = i;
                while(j>0 && arr[j]>arr[j-1])
                {
                    arr[j] = arr[j - 1];
                    j--;
                }
                arr[j] = temp;
            }
            
        }

        private static int[]  takeArrayInput()
        {
            Console.WriteLine("Enter the Size of the array");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] arr = new int[size];
            for(int i=0;i<size;i++)
            {
                Console.Write("Enter the value at" + i);
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }

            return arr;

        }

        private static void validateUSDN()
        {
            Console.WriteLine("Enter Your University Seat Number");
            string usdn = Convert.ToString(Console.ReadLine());
            if (validating(usdn))
            {
                Console.WriteLine("SUCCESS");
            }
            else
            {
                Console.WriteLine("FAILURE");
            }
        }

        private static Boolean validating(string usdn)
        {
            Boolean res;
            if (usdn.Length <10 || usdn.Length>10)
            {
                res = false;

            }
            else
            {
                if(usdn[0]=='1' || usdn[0]=='2')
                {
                    if((int)usdn[1]>=65 && (int)usdn[1]<=90)
                    {
                       if((int)usdn[2]>=65 && (int)usdn[2]<=90)
                        {
                            if((int)usdn[3]>=48 && (int)usdn[3]<=57)
                            {
                                if ((int)usdn[4] >= 48 && (int)usdn[4] <= 57)
                                {
                                    if ((int)usdn[5] >= 67 && (int)usdn[5] <= 77)
                                    {
                                        string temp = "" + usdn[5] + usdn[6];
                                        Console.WriteLine(temp);
                                        if (temp.Equals("CS") || temp.Equals("IS") || temp.Equals("EC")||temp.Equals("ME"))
                                        {
                                            if(usdn[7] >= 48 && usdn[7]<=57)
                                            {
                                                if(usdn[8] >=48 && usdn[8]<=57)
                                                {
                                                    if(usdn[9]>=48 && usdn[9]<=57)
                                                    {
                                                        res = true;
                                                    }
                                                    else
                                                    {
                                                        res = false; 
                                                    }

                                                }
                                                else
                                                {
                                                    res = false;
                                                }
                                            }
                                            else
                                            {
                                                res = false;
                                            }
                                        }
                                        else
                                        {
                                            res = false;
                                        }

                                    }
                                    else
                                    {
                                        res = false;
                                    }
                                }
                                else
                                {
                                    res = false;
                                }
                            }
                            else
                            {
                                res = false;
                            }

                        }
                        else
                        {
                            res = false;
                        }
                    }
                    else
                    {
                        res = false;
                    }
                }
                else
                {
                    res = false;
                }
            }
            return res;
        }
    }
}
